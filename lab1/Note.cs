﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1
{
    class Note
    {
        public static int count;

        public int Id { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Name2 { get; set; }
        public string TelNum { get; set; }
        public string Country { get; set; }
        public DateTime BirthDate { get; set; }
        public string Organisation { get; set; }
        public string Profession { get; set; }
        public string Others { get; set; }

        public Note()
        {
            count++;
            Id = count;

        }

        ~Note()
        {
            count--;
            foreach (KeyValuePair<int,Note> values in Notebook.notes)
            {
                if (values.Key > Id)
                {
                    Note note = values.Value;
                    note.Id--;
                }
            }
        }

        public override string ToString()
        {
            string date;
            if (BirthDate.Equals(DateTime.MinValue))
            {
                date = "";
            }
            else
            {
                date = BirthDate.ToLongDateString();
            }

            string str = "__________________\n" +
                "Фамилия: " + Surname + "\nИмя: " + Name +
                "\nОтчество:" + Name2 + "\nНомер телефона: : " + TelNum +
                "\nСтрана: " + Country + "\nДата рождения: " + date +
                "\nОрганизация: " + Organisation + "\nДолжность: " + Profession +
                "\nПрочие заметки: " + Others + "\n__________________";
            return str;
        }
    }
}
