﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace lab1

{
    class Notebook
    {
        static public Dictionary<int, Note> notes = new Dictionary<int, Note>();
        static public string[,] names = {{"Surname", "фамилия"},
            { "Name", "имя" } ,
        { "Name2", "отчество"} ,
        { "TelNum", "номер телефона" },
        { "Country", "страна"},
        { "BirthDate", "дата рождения"},
        { "Organisation", "организация"},
        { "Profession", "профессия" },
        { "Others", "прочие заметки"}  };
        static void Main(string[] args)
        {
            Note note1 = new Note() {Surname = "Vavilov", Name = "Konstantin", TelNum = "81234567890", Country = "Russia", BirthDate = DateTime.Parse("07.12.1998") };
            Note note2 = new Note() { Surname = "Vavilov", Name = "Petr", TelNum = "81234567891", Country = "Russia" };
            Note note3 = new Note() { Surname = "Simonov", Name = "Petr", TelNum = "81234567892", Country = "Russia" };
            Note note4 = new Note() { Surname = "Ivanov", Name = "Ivan", TelNum = "81234567893", Country = "Russia" };
            Note note5 = new Note() { Surname = "Petrov", Name = "Lev", TelNum = "81234567894", Country = "Russia" };

            notes.Add(note1.Id, note1);
            notes.Add(note3.Id, note3);
            notes.Add(note4.Id, note4);
            notes.Add(note5.Id, note5);
            notes.Add(note2.Id, note2);





            while (true)
            {
                StartInteraction();

                string action = Console.ReadLine();
                if (action.Equals("1"))
                {
                    CreateNewNote();
                    continue;

                }
                if (action.Equals("2"))
                {
                    EditNote();
                    continue;
                }
                if (action.Equals("3"))
                {
                    DeleteNote();
                    continue;

                }
                if (action.Equals("4"))
                {
                    ReadNote();
                    continue;

                }
                if (action.Equals("5"))
                {
                    ShowAllNotes();
                    continue;

                }
                if (action.Equals("6"))
                {
                    Console.WriteLine("Спасибо за пользование приложением \"Записная книжка\"!. До свидания!" +
                        "\nДля выхода нажмите любую клавишу.");
                    Console.ReadKey();
                    break;
                }
                else
                {
                    WrongAction();
                    BeforeEnd();
                }
            }

            Console.ReadKey();
        }
        
        // BASIC ACTIONS //
        static void CreateNewNote()
        {
            Note note = new Note();

            Console.WriteLine("Заполните данные о контакте. \nПоля имя, фамилия, номер телефона и страна обязательны для заполнения!\n\n");

            for (int i = 0; i < 9; i++)
            {
                ChangeName(note, names[i, 0], names[i, 1]);
            }
            notes.Add(note.Id, note);
        }
        static void EditNote()
        {
            Console.Clear();

            if (notes.Count != 0)
            {

                Note note = FindNote();
                if (note != null)
                {
                    Console.WriteLine("Вы хотите изменить следующую запись: \n");
                    Console.WriteLine(note);
                    Console.WriteLine();
                    for (int i = 0; i < 9; i++)
                    {
                        Console.WriteLine("Для изменения поля {0} нажмите {1}.", names[i, 1], i);
                    }
                    Console.WriteLine("Для выхода в главное меню нажмите 9.");
                    Console.WriteLine();
                    while (true)
                    {
                        try
                        {
                            int index = int.Parse(Console.ReadLine());
                            if (index == 9)
                            {

                                break;
                            }
                            ChangeName(note, names[index, 0], names[index, 1]);

                            Console.WriteLine("Вы успешно изменили запись:\n");
                            Console.WriteLine(note);
                            BeforeEnd();
                            break;
                        }
                        catch (Exception e)
                        {
                            WrongAction();
                        }
                    }

                }
                else
                {
                    Console.WriteLine("Не удалось найти нужную запись\n\n");
                    BeforeEnd();
                }
            }
            else
            {
                EmptyDic();
            }

        }
        static void DeleteNote()
        {
            Console.Clear();

            if (notes.Count != 0)
            {
                Note note = FindNote();

                if (note != null)
                {
                    int id = note.Id;

                    Console.WriteLine("Была удалена следующая запись: \n");
                    Console.WriteLine(note);

                    notes.Remove(id);


                    BeforeEnd();
                }

            }
            else
            {
                EmptyDic();

            }

        }
        static void ReadNote()
        {
            Console.Clear();

            if (notes.Count != 0)
            {

                Note note = FindNote();
                if (note != null)
                {
                    Console.WriteLine(note);
                    BeforeEnd();
                }
                else
                {
                    NotFounded();
                    BeforeEnd();
                }


            }
            else
            {
                EmptyDic();
            }
        }
        static void ShowAllNotes()
        {
            Console.Clear();
            Console.WriteLine("Ваша записная книжка:\n\n");
            if (notes.Count != 0)
            {
                foreach (KeyValuePair<int, Note> values in notes)
                {
                    Console.WriteLine(values.Value);
                }

                BeforeEnd();
            }
            else
            {
                EmptyDic();
            }

        }

        // MESSAGES //
        static void StartInteraction()
        {
            Console.Clear();
            Console.WriteLine("Добро пожаловать в \"Записную книжку\" !\n\n");
            Console.WriteLine("Для создания новой записи нажмите 1.");
            Console.WriteLine("Для редактирования записи нажмите 2.");
            Console.WriteLine("Для удаления записи нажмите 3.");
            Console.WriteLine("Для прочтения записи нажмите 4.");
            Console.WriteLine("Для просмотра всех записей нажмите 5.");
            Console.WriteLine("Для выхода нажмите 6.");
            Console.WriteLine();


        }
        static void MessageNull(string rusName)
        {
            Console.WriteLine("Вы не присвоили полю {0} никакого значения.\n", rusName);
        }
        static void OnlyNum()
        {
            Console.WriteLine("Это поле должно содержать только цифры! Введите еще раз.\n");

        }
        static void NoLetetrError()
        {
            Console.WriteLine("Это поле должно содержать только буквы! Введите еще раз.\n");
        }
        static void NotNullError(string rusNAme)
        {
            Console.WriteLine("Поле {0} не может быть пустым. Введите еще раз.\n", rusNAme);
        }
        static void WrongDate()
        {
            Console.WriteLine("Неверный формат даты. Введите еще раз.\n");

        }
        static void NotFounded()
        {
            Console.WriteLine("Не удалось найти нужную запись\n\n");
        }
        static void EmptyDic()
        {
            Console.WriteLine("Ваша записная книжка пуста.");
            Console.WriteLine("Для продолжения работы с записной книжкой нажмите любую клавишу.");
            Console.ReadKey();
        }
        static void WrongAction()
        {
            Console.WriteLine("Недопустимое действие. Попробуйте еще раз.\n");
        }
        static void BeforeEnd()
        {
            Console.WriteLine();
            Console.WriteLine("Для продолжения работы с записной книжкой нажмите любую клавишу.");
            Console.ReadKey();
        }

        // FUNCTIONS //
        static string CorrectName(string name)
        {
            name = name.ToLower();
            name = name.Substring(0, 1).ToUpper() + name.Remove(0, 1);
            return name;
        }
        static bool IsDig(string name)
        {
            bool flag = false;
            foreach(char c in name)
            {
                if (char.IsDigit(c))
                {
                    flag = true;
                }
            }
            return flag;
        }
        static bool IsLet(string name)
        {
            bool flag = true;
            foreach (char c in name)
            {
                if (!char.IsLetter(c) || char.IsPunctuation(c) || char.IsSymbol(c))
                {
                    flag = false;
                    
                }
            }
            return flag;
        }
        static Note FindNote()
        {


            List<Note> foundedNotes = new List<Note>();
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Выберите способ, по которому будет осуществляться поиск записей.\n\n");
                for (int i = 0; i < 9; i++)
                {
                    Console.WriteLine("Для поиска по полю {0} нажмите {1}.",  names[i,1], i );
                }
                Console.WriteLine();
                string action = Console.ReadLine();
                try
                {
                    int act = int.Parse(action);
                    if (act < 0 || act > 8)
                    {
                        throw new Exception();
                    }
                    for (int i = 0; i < 9; i++)
                    {
                        if (action.Equals(i.ToString()))
                        {
                            Console.WriteLine("Введите поле {0}", names[i, 1]);
                            string property = names[i, 0];
                            string name = Console.ReadLine();
                            if (property.Equals("BirthDate"))
                            {
                                try
                                {
                                    DateTime date = DateTime.Parse(name);
                                    foreach(KeyValuePair<int, Note> values in notes)
                                    {
                                        if (values.Value.BirthDate.Equals(date))
                                        {
                                            foundedNotes.Add(values.Value);
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                    return null;
                                }
                            }
                            else
                            {
                                name = CorrectName(name);
                                foreach (KeyValuePair<int, Note> values in notes)
                                {

                                    if (values.Value.GetType().GetProperty(property).GetValue(values.Value).Equals(name))
                                    {
                                        foundedNotes.Add(values.Value);
                                    }
                                }
                            }
                            
                            if (foundedNotes.Count == 0)
                            {
                                return null;
                            }
                            else
                            {
                                return ReturnFoundedNote(foundedNotes);
                            }



                        }
                    }
                }
                catch (Exception e)
                {
                    WrongAction();
                    BeforeEnd();
                }
            }
        }
        static Note ReturnFoundedNote(List<Note> foundedNotes)
        {
            Console.WriteLine("Было найдено {0} записей.\n\n", foundedNotes.Count);

            for (int i = 0; i < foundedNotes.Count; i++)
            {
                Console.WriteLine("№    " + i);
                Console.WriteLine(foundedNotes[i]);
            }
            Console.WriteLine();
            for (int i = 0; i < foundedNotes.Count; i++)
            {
            Console.WriteLine("Для выбора {0} записи нажмите {0}.", i);
            }
            Console.WriteLine();

            string act = Console.ReadLine();
            try
                {
                int action = int.Parse(act);
                if (foundedNotes.Count > action)
                {
                    Console.WriteLine("Была выбрана запись {0}.", action);
                    return foundedNotes[action];
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                
                return null;
            }
        }
        static void ChangeName(Note note, string property, string rusName)
        {
            Console.WriteLine("Заполните поле " + rusName +": ");
            if (property.Equals("TelNum"))
            {
                Console.WriteLine("(Формат номера 81234567890)");
                while (true)
                {
                    string num = Console.ReadLine();
                    if (IsDig(num))
                    {
                        if (num.Length == 11)
                        {
                            note.GetType().GetProperty(property).SetValue(note, num);
                            break;
                        }
                        else
                        {
                            Console.WriteLine("Номер телефона должен сожержать 11 цифр. Введите еще раз.");
                        }
                        
                    }
                    else
                    {
                        if (num.Equals(""))
                        {
                            NotNullError(rusName);
                        }
                        else
                        {
                            OnlyNum();
                        }
                    }
                }

            }
            else if (property.Equals("BirthDate"))
            {
                Console.WriteLine("(Формат даты рождения 01.01.2001)");

                while (true)
                {
                    string name = Console.ReadLine();
                    
                    try
                    {
                        DateTime date = DateTime.Parse(name);
                        note.GetType().GetProperty(property).SetValue(note, date);
                        break;

                    }
                    catch (Exception e)
                    {
                        WrongDate();
                    }
                    if (name == "")
                    {
                        MessageNull(rusName);
                        DateTime date = DateTime.MinValue;
                        note.GetType().GetProperty(property).SetValue(note, date);
                        break;
                    }

                }

            }
            else if (property.Equals("Surname") || property.Equals("Name") || property.Equals("Country"))
            {

                while (true)
                {
                    string name = Console.ReadLine();
                    if (IsLet(name))
                    {
                        if (name.Equals(""))
                        {
                            NotNullError(rusName);
                            continue;
                        }
                        name = CorrectName(name);
                        note.GetType().GetProperty(property).SetValue(note, name);
                        break;
                          
                    }
                    else
                    {
                        NoLetetrError();
                    }                                        
                }
            }
            else
            {
                while (true)
                {
                    string name = Console.ReadLine();
                    if (IsLet(name))
                    {
                        if (name.Equals(""))
                        {
                            MessageNull(rusName);
                        }
                        else
                        {
                            name = CorrectName(name);
                        }
                        
                        note.GetType().GetProperty(property).SetValue(note, name);
                        break;

                    }
                    else
                    {
                        NoLetetrError();
                    }
                }
            }

            
        }
    }
}
